module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
          dist: {
            options: {
              style: 'compressed',
              compass: true
            },
            files: {
              'css/icons.css': 'sass/icon.scss',
            }
          }
        },
        watch: {
            files: ['sass/**/*.scss'],
            tasks: ['sass'],
            options: {
                nospawn: true,
                livereload: 8122,
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['sass']);
};
